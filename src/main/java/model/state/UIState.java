package model.state;

public class UIState {

  private int scale = 1;
  private int maxAmplitude = 2;
  private boolean actionsVisible = true;

  public int getScale() { return scale; }

  public void setScale(int scale) { this.scale = scale; }

  public int getMaxAmplitude() { return maxAmplitude; }

  public void setMaxAmplitude(int maxAmplitude) { this.maxAmplitude = maxAmplitude; }

  public boolean actionsVisible() { return actionsVisible; }

  public void toggleActionsVisibility() { actionsVisible = !actionsVisible; }

}
