package model.state;

import controller.App;
import model.element.Element;
import model.element.Emitter;
import model.element.Receiver;

import java.util.LinkedList;
import java.util.List;

public class Base {

  private final App APP_CTRL;
  private final AppState APP_STATE;
  private final UIState UI_STATE;

  private final LinkedList<Receiver> receivers = new LinkedList<>();
  private final LinkedList<Emitter> emitters = new LinkedList<>();
  private final LinkedList<Element> elements = new LinkedList<>();

  private boolean pause = true;

  public Base(App app) {
    APP_CTRL = app;
    APP_STATE = new AppState();
    UI_STATE = new UIState();
  }

  public void addEmitter(Emitter emitter) {
    emitters.add(emitter);
    elements.add(emitter);
  }

  public void addReceiver(Receiver receiver) {
    receivers.add(receiver);
    elements.add(receiver);
  }

  public App getAppCtrl() { return APP_CTRL; }

  public List<Receiver> getReceivers() { return receivers; }

  public List<Emitter> getEmitters() { return emitters; }

  public List<Element> getElements() { return elements; }

  public void togglePause() { pause = !pause; }

  public boolean isPaused() { return pause; }

  public AppState getAppState() { return APP_STATE; }

  public UIState getUIState() { return UI_STATE; }

}
