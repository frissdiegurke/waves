package model;

import model.element.Emitter;
import model.geometry.Movable;

public class Wave extends Movable {

  private double originX = 0, originY = 0;
  private double originRadius = 0, originSqRadius = 0;

  private double frequency; // periods per ms
  private double propagationSpeed; // px per ms
  private double frequencyTimes2Pi; // 2 * PI * frequency  // saves 2 multiplications per pixel per frame

  private double minRadius = 0, minSqRadius = 0;

  private int birthAgo = 0;

  private boolean initial = true;
  private boolean detached = false;

  public Wave(double originX, double originY, double frequency, double propagationSpeed) {
    super(originX, originY, 0, 0);
    setFrequency(frequency);
    setPropagationSpeed(propagationSpeed);
  }

  public void detach(Emitter emitter) {
    setX(emitter.getX());
    setY(emitter.getY());
    setVX(emitter.getVX());
    setVY(emitter.getVY());
    detached = true;
  }

  public void update(double time) {
    if (initial) {
      originX = this.getX();
      originY = this.getY();
      initial = false;
    }
    birthAgo += time;
    originRadius += propagationSpeed * time;
    originSqRadius = originRadius * originRadius;
    if (detached) {
      super.update(time);
      minRadius += propagationSpeed * time;
      minSqRadius = minRadius * minRadius;
    }
  }

  public boolean isInitial() { return initial; }

  public double getOriginX() { return originX; }

  public double getOriginY() { return originY; }

  public double getOriginSqRadius() { return originSqRadius; }

  public double getFrequencyTimes2Pi() { return frequencyTimes2Pi; }

  public int getBirthAgo() { return birthAgo; }

  public double getFrequency() { return frequency; }

  public void setFrequency(double frequency) { frequencyTimes2Pi = (this.frequency = frequency) * 2 * Math.PI; }

  public double getPropagationSpeed() { return propagationSpeed; }

  public void setPropagationSpeed(double propagationSpeed) { this.propagationSpeed = propagationSpeed; }

}
