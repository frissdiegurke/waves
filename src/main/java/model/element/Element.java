package model.element;

import model.geometry.Movable;

import java.awt.*;

public class Element extends Movable {

  private final Color COLOR;

  public Element(Color color, double x, double y, double vx, double vy) {
    super(x, y, vx, vy);
    COLOR = color;
  }

  public Color getColor() { return COLOR; }

}
