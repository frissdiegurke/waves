package model.element;

import model.Wave;

import java.awt.*;
import java.util.LinkedList;

public class Emitter extends Element {

  private Wave attachedWave;
  private LinkedList<Wave> detachedWaves = new LinkedList<>();
  private boolean visible = true;

  public Emitter(Color color, double x, double y, double vx, double vy) {
    super(color, x, y, vx, vy);
    attachedWave = new Wave(x, y, 0.001, 0.1);
  }

  public void update(double time) {
    attachedWave.update(time);
    for (Wave wave : detachedWaves) { wave.update(time); }
    super.update(time);
  }

  public void move(double dx, double dy) {
    super.move(dx, dy);
    if (attachedWave.isInitial()) { attachedWave.move(dx, dy); }
  }

  public void setX(double x) {
    super.setX(x);
    if (attachedWave.isInitial()) { attachedWave.setX(x); }
  }

  public void setY(double y) {
    super.setY(y);
    if (attachedWave.isInitial()) { attachedWave.setY(y); }
  }

  public void setVisibility(boolean visible) { this.visible = visible; }

  public boolean isVisible() { return visible; }

  public Wave getAttachedWave() { return attachedWave; }

  public LinkedList<Wave> getDetachedWaves() { return detachedWaves; }

  private void beginModification() {
    if (!attachedWave.isInitial()) {
      attachedWave.detach(this);
      detachedWaves.add(attachedWave);
      attachedWave = new Wave(this.getX(), this.getY(), 0.001, 0.1);
      // todo add offset for amplitude calculation to get smooth transition between the waves
    }
  }

  public void setFrequency(double frequency) {
    beginModification();
    this.attachedWave.setFrequency(frequency);
  }

  public void setPropagationSpeed(double spreadingSpeed) {
    beginModification();
    this.attachedWave.setPropagationSpeed(spreadingSpeed);
  }

  public double getFrequency() { return attachedWave.getFrequency(); }

  public double getPropagationSpeed() { return attachedWave.getPropagationSpeed(); }

}
