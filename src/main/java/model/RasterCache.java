package model;

public class RasterCache {

  private int[][] cache;
  private int scale = 0;

  public int[] get(int amplitude) { return cache[amplitude]; }

  public void setScale(int scale) {
    if (scale == this.scale) { return; }
    this.scale = scale;
    int square = scale * scale;
    cache = new int[256][square];
    for (int i = 0; i < 256; i++) { for (int j = 0; j < square; j++) { cache[i][j] = i; } }
  }

}
