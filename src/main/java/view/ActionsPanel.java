package view;

import model.state.Base;

import java.awt.*;

public class ActionsPanel extends Panel {

  public static final Dimension MINIMUM_SIZE = new Dimension(200, 400);
  public static final Dimension PADDING = new Dimension(5, 3);

  private final ActionsScrollPane scrollPane;

  public ActionsPanel(Base state) {
    setMinimumSize(MINIMUM_SIZE);
    setBackground(Color.LIGHT_GRAY);
    add(scrollPane = new ActionsScrollPane(state));
  }

  public void updateState() { scrollPane.updateState(); }

  public void paint(Graphics g) {
    g.setColor(Color.BLUE);
    paintBorder(g);
    int y = paintInfo(g) + PADDING.height * 2;
    scrollPane.setBounds(1, y, getWidth() - 2, getHeight() - y - PADDING.height);
    scrollPane.doLayout();
  }

  private void paintBorder(Graphics g) {
    g.drawRect(0, 0, getWidth() - 1, getHeight() - 1);
  }

  private int paintInfo(Graphics g) {
    String info = "waves 1.0";
    FontMetrics fontMetrics = g.getFontMetrics();
    int y = fontMetrics.getHeight();
    g.drawString(info, getWidth() - fontMetrics.stringWidth(info) - PADDING.width, y);
    return y;
  }

}
