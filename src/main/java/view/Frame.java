package view;

import model.state.Base;
import view.listener.FrameWindowListener;
import view.listener.GlobalContainerListener;
import view.listener.GlobalKeyListener;

import java.awt.*;

public class Frame extends java.awt.Frame {

  private static final Dimension MINIMUM_SIZE = new Dimension(400, 400);

  private final Base STATE;
  private final Canvas CANVAS;
  private final ActionsPanel ACTIONS;

  public Frame(Base state, Canvas canvas, ActionsPanel actions) {
    STATE = state;
    CANVAS = canvas;
    ACTIONS = actions;

    // listeners
    addWindowListener(new FrameWindowListener(STATE));
    new GlobalContainerListener(this, new GlobalKeyListener(STATE));

    setBackground(new Color(128, 128, 128));
    CANVAS.setLocation(0, 0);

    add(CANVAS);
    add(ACTIONS);

    setMinimumSize(MINIMUM_SIZE);
    setSize(MINIMUM_SIZE);

    setLayout(null);
  }

  public void doLayout() {
    int w = getWidth(), h = getHeight();
    if (STATE.getUIState().actionsVisible()) {
      int actionsWidth = ACTIONS.getMinimumSize().width;
      ACTIONS.setBounds(w - actionsWidth, 0, actionsWidth, h);
      ACTIONS.doLayout();
      ACTIONS.setVisible(true);
      CANVAS.setSize(w - actionsWidth, h);
      CANVAS.doLayout();
      CANVAS.setVisible(true);
    } else {
      CANVAS.setSize(w, h);
      CANVAS.doLayout();
      CANVAS.setVisible(true);
      ACTIONS.setVisible(false);
    }
  }

}
