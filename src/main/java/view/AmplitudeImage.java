package view;

import model.RasterCache;
import model.WorkingLock;
import model.state.Base;
import util.calc.Amplitude;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class AmplitudeImage {

  private static final int POOL_SIZE = Runtime.getRuntime().availableProcessors();
  private static final ExecutorService POOL = Executors.newFixedThreadPool(POOL_SIZE);

  private class Worker implements Runnable {

    private final WritableRaster raster;
    private final int yStart, yCap, width;
    private final boolean last;

    private int fieldY;

    private Worker(WritableRaster raster, int width, int fieldY, int yStart, int yCap, boolean last) {
      this.raster = raster;
      this.fieldY = fieldY;
      this.yStart = yStart;
      this.yCap = yCap;
      this.width = width;
      this.last = last;
    }

    public void run() {
      int scale = STATE.getUIState().getScale();
      int pWidth = width - scale, pHeight = last ? yCap - scale : yCap;
      int x, y, fieldX;
      for (y = yStart; y < pHeight; y += scale) {
        for (x = fieldX = 0; x < pWidth; x += scale) {
          raster.setPixels(x, y, scale, scale, rasterCache.get(getAmplitudeBrightness(fieldX, fieldY)));
          fieldX++;
        }
        raster.setPixels(x, y, width - x, scale, rasterCache.get(getAmplitudeBrightness(fieldX, fieldY)));
        fieldY++;
      }
      if (last) {
        for (x = fieldX = 0; x < pWidth; x += scale) {
          raster.setPixels(x, y, scale, yCap - y, rasterCache.get(getAmplitudeBrightness(fieldX, fieldY)));
          fieldX++;
        }
        raster.setPixels(x, y, width - x, yCap - y, rasterCache.get(getAmplitudeBrightness(fieldX, fieldY)));
      }
      synchronized (LOCK) { if (--LOCK.awaiting == 0) { LOCK.notify(); } }
    }

  }

  private final Base STATE;
  private final Point ZERO;
  private final WorkingLock LOCK = new WorkingLock();
  private final RasterCache rasterCache = new RasterCache();

  public AmplitudeImage(Base state, Point zero) {
    STATE = state;
    ZERO = zero;
  }

  public BufferedImage getImage(int width, int height) {
    BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);
    WritableRaster raster = image.getRaster();
    synchronized (LOCK) {
      synchronized (STATE) {
        int scale = STATE.getUIState().getScale();
        rasterCache.setScale(scale);
        int rowsPerWorker = (int) Math.ceil(height / (double) scale / POOL_SIZE);
        int yStart = 0, fieldY = 0, yCap;
        boolean last = false;
        while (!last) {
          yCap = yStart + rowsPerWorker * scale;
          if (yCap >= height) {
            last = true;
            yCap = height;
          }
          LOCK.awaiting++;
          POOL.submit(new Worker(raster, width, fieldY, yStart, yCap, last));
          yStart = yCap;
          fieldY = yStart / scale;
        }
        try { LOCK.wait(); } catch (InterruptedException e) { e.printStackTrace(); }
      }
    }
    return image;
  }

  private int getAmplitudeBrightness(int x, int y) {
    double amplitude = Amplitude.getInterference(STATE.getEmitters(), ZERO.x + x, ZERO.y + y);
    int brightness = (int) (127.5 + amplitude * 127.5 / STATE.getUIState().getMaxAmplitude());
    return brightness < 0 ? 0 : brightness > 255 ? 255 : brightness;
  }

  public void exit() {
    POOL.shutdown(); // Disable new tasks from being submitted
    try {
      // Wait a while for existing tasks to terminate
      if (!POOL.awaitTermination(3, TimeUnit.SECONDS)) {
        POOL.shutdownNow(); // Cancel currently executing tasks
        // Wait a while for tasks to respond to being cancelled
        if (!POOL.awaitTermination(5, TimeUnit.SECONDS))
          System.err.println("Pool did not terminate");
      }
    } catch (InterruptedException ie) {
      // (Re-)Cancel if current thread also interrupted
      POOL.shutdownNow();
      // Preserve interrupt status
      Thread.currentThread().interrupt();
    }
  }

}
