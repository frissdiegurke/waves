package view.listener;

import java.awt.*;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import java.awt.event.KeyListener;

public class GlobalContainerListener implements ContainerListener {

  private final KeyListener GLOBAL_KEY_LISTENER;

  public GlobalContainerListener(Container container, KeyListener globalKeyListener) {
    GLOBAL_KEY_LISTENER = globalKeyListener;
    container.addContainerListener(this);
    container.addKeyListener(GLOBAL_KEY_LISTENER);
  }

  public void componentAdded(ContainerEvent e) { addKeyAndContainerListenerRecursively(e.getChild()); }

  public void componentRemoved(ContainerEvent e) {}

  private void addKeyAndContainerListenerRecursively(Component component) {
    component.addKeyListener(GLOBAL_KEY_LISTENER);
    if (component instanceof Container) {
      Container container = (Container) component;
      container.addContainerListener(this);
      for (Component child : container.getComponents()) { addKeyAndContainerListenerRecursively(child); }
    }
  }

}
