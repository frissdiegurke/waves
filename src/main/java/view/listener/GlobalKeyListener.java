package view.listener;

import model.state.Base;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class GlobalKeyListener implements KeyListener {

  private final Base STATE;

  public GlobalKeyListener(Base state) { STATE = state; }

  public void keyTyped(KeyEvent e) {}

  public void keyPressed(KeyEvent e) {
    if (e.isControlDown() && !e.isAltDown() && !e.isShiftDown()) {
      switch (e.getKeyCode()) {
        case KeyEvent.VK_Q:
          STATE.getAppState().stopRunning();
          return;
      }
    }
    if (!e.isControlDown() && e.isAltDown() && !e.isShiftDown()) {
      switch (e.getKeyCode()) {
        case KeyEvent.VK_A:
          STATE.getUIState().toggleActionsVisibility();
          STATE.getAppCtrl().getUICtrl().doLayout();
          break;
      }
    }
  }

  public void keyReleased(KeyEvent e) {}

}
