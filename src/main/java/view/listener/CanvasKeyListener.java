package view.listener;

import model.state.Base;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class CanvasKeyListener implements KeyListener {

  private final Base STATE;

  public CanvasKeyListener(Base state) { STATE = state; }

  public void keyTyped(KeyEvent e) {}

  public void keyPressed(KeyEvent e) {
    switch (e.getKeyCode()) {
      case KeyEvent.VK_SPACE:
        STATE.togglePause();
        STATE.getAppCtrl().updateState();
        break;
    }
  }

  public void keyReleased(KeyEvent e) {}

}
