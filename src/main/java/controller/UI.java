package controller;

import model.state.Base;
import model.element.Element;
import view.ActionsPanel;
import view.Canvas;
import view.Frame;

public class UI {

  private final Base STATE;

  private Frame FRAME;
  private Canvas CANVAS;
  private ActionsPanel ACTIONS;

  public UI(Base state) {
    STATE = state;
  }

  public void createWindow() {
    createComponents();
    FRAME.setVisible(true);
    CANVAS.requestFocus();
  }

  private void createComponents() {
    CANVAS = new Canvas(STATE);
    ACTIONS = new ActionsPanel(STATE);
    FRAME = new Frame(STATE, CANVAS, ACTIONS);
  }

  public void update(double time) {
    if (STATE.isPaused()) { return; }
    for (Element element : STATE.getElements()) { element.update(time); }
    repaint();
  }

  public void updateState() { ACTIONS.updateState(); }

  public void exit() {
    CANVAS.exit();
    FRAME.dispose();
  }

  public void repaint() {
    CANVAS.update();
    CANVAS.repaint();
  }

  public void doLayout() { FRAME.doLayout(); }

}
