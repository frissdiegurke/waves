package controller;

import model.element.Emitter;
import model.state.Base;

import java.awt.*;

public class App {

  private static final int TARGET_FPS = 60;
  private static final long OPTIMAL_TIME = 1000000000 / TARGET_FPS;

  public static void main(String[] args) { new App(); }

  private final Base STATE;
  private final UI UI_CTRL;

  private App() {
    STATE = new Base(this);
    UI_CTRL = new UI(STATE);

    populate(); // to be removed

    startUI();
    updateState();

    startLoop();
    exit();
  }

  private void populate() {
    // todo replace with pre-defined colors
    Color c1 = new Color((int) (Math.random() * 256), (int) (Math.random() * 256), (int) (Math.random() * 256));
    Color c2 = new Color((int) (Math.random() * 256), (int) (Math.random() * 256), (int) (Math.random() * 256));
    STATE.addEmitter(new Emitter(c1, 110, 110, 0.005, 0.05));
    STATE.addEmitter(new Emitter(c2, 300, 300, -0.02, 0.003));
  }

  public void updateState() { UI_CTRL.updateState(); }

  public UI getUICtrl() { return UI_CTRL; }

  private void startUI() { new Thread(UI_CTRL::createWindow).run(); }

  private void startLoop() {
    long lastUpdateTime = System.nanoTime();

    while (STATE.getAppState().isRunning()) {
      long now = System.nanoTime();
      long updateNanoseconds = now - lastUpdateTime;

      if (updateNanoseconds < OPTIMAL_TIME) {
        try {
          Thread.sleep((OPTIMAL_TIME - updateNanoseconds) / 1000000);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      } else {
        lastUpdateTime = now;
        update(updateNanoseconds / 1000000d);
        if (!STATE.isPaused()) { updateState(); }
      }
    }
  }

  private void update(double time) { UI_CTRL.update(time); }

  private void exit() { UI_CTRL.exit(); }

}
